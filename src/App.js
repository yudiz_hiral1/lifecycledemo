// import logo from './logo.svg'
import './App.css'
import React from 'react'
// import App1 from './componentDidMount'
// import ComponentWillUnmount from './componentWillUnmount'
import LifeCycle from './LifeCycle'

function App () {
  return (
    <div className="App">
      {/* <App1 /> */}
      {/* <ComponentWillUnmount /> */}
      <LifeCycle text='Rohan'/>
    </div>
  )
}

export default App
