import React, { Component } from 'react'

class App1 extends Component {
  constructor () {
    super()
    this.state = {
      text: 'This is Constructor Method'
    }
    console.log(this.state.text)
  }

  componentDidMount () {
    setTimeout(() => {
      this.setState({ text: 'This is ComponentDidMount Method' })
    }, 5000)
  }

  render () {
    return (
        <div>
            <h1> Life Cycle Method </h1>
            <h3> { this.state.text }</h3>
        </div>
    )
  }
}
export default App1
