import React, { Component } from 'react'

class ComponentWillUnmount extends Component {
  constructor () {
    super()
    this.state = {
      show: true
    }
  }

  render () {
    return (
            <div>
                <h3> componentWillMount </h3>
                <p> {this.state.show}</p>
                <button onClick={''}> Click Here </button>
            </div>
    )
  }
}
export default ComponentWillUnmount
