import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Person from './person'

class LifeCycle extends Component {
  constructor (props) {
    super(props)
    this.state = {
      text: 'Hiral Kakrecha',
      delete: false
    }
  }

  componentDidMount () {
    setTimeout(() => {
      this.setState({ text: 'to current state' })
    }, 5000)
  }

  componentDidUpdate (prevProps, prevState) {
    // eslint-disable-next-line react/prop-types
    if (prevProps.text !== prevState.text) {
      document.getElementById('statechange').innerHTML = prevState.text
    }
  }

  setData = () => {
    this.setState({ text: 'Hiral' })
  }

  setdelet = () => {
    // this.setState({ delete: true })
    this.setState({ delete: !this.state.delete })
  }

  render () {
    return (
            <div>
             <h1> Hello </h1>
             <h3> { this.state.text } <br/> { this.props.text }</h3>
             <button onClick={this.setData}> componentDidUpdate </button>
             <p id='statechange'></p>
             <div>
             <button onClick={this.setdelet}> componentWillUnmount </button>
             {this.state.delete ? null : <Person/>}
             </div>
            </div>
    )
  }
}

LifeCycle.propTypes = {
  text: PropTypes.string.isRequired
}

export default LifeCycle
